# CheatSheet

Personal Cheat Sheet and other snippet

## Requirement

This service is started by a docker compose, saw the compliance between
docker-compose [documentation](https://docs.docker.com/compose/compose-file/compose-file-v3/#dockerfile) and
the [docker-compose file](./docker-compose.yml)

## RUN and build

### backend

```shell
$ docker-compose up 
```

The docker of the backend expose an endpoint on the port 8080

#### Configuration
The docker uses some environment variable, as follows:

|Environment variable| Description                                                                                                                   | Format | Optional/Mandatory | Default value                       |
|-----|-------------------------------------------------------------------------------------------------------------------------------|--------|-------------------|-------------------------------------|
|PROJECT_ID| Identifier of the project under gitlab, could be retrieve, in                                                                 | String | Mandatory         | N/A                                 |
|GITLAB_PUBLIC_TOKEN| Gitlab token with api read right  [gitlab documentation](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) | String | Mandatory         | N/A                                 |
|GIT_URL| Url of the client you want to adress                                                                                          | String | Optional          | https://gitlab.com/api/v4/projects/ |

Get Project id ![](./src/docs/projectId.gif)