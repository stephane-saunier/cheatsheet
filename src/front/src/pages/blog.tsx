import * as React from 'react'
import { graphql } from 'gatsby'

import Layout from '../components/layout'



const BlogPage = ({ data }) => {
  return (
    <Layout pageTitle="My Blog Posts">
      {
        data.allMdx.nodes.map((node) => (
          <a href={`/blog/${node.slug}`}>
            <div className="mt-2 tile is-parent">
              <article className="tile is-child box" key="{node.id}">
                <h2 className="title is-5">
                  {node.frontmatter.title}
                </h2>
                <p>Description : {node.frontmatter.abstract}</p>
                <div className="columns mt-1">
                  <p className="column is-half">Posted: {node.frontmatter.date}</p>
                  <span className="column is-quarter"></span>
                  <div className="tags has-addons">
                    <span className="tag is-black">{node.frontmatter.tag.split(",").length > 0 ? node.frontmatter.tag.split(",")[0] : ''}</span>
                    <span className="tag is-primary">{node.frontmatter.tag.split(",").length > 1 ? node.frontmatter.tag.split(",")[1] : ''}</span>
                  </div>
                </div>
              </article>
            </div>
          </a>
        ))
      }
    </Layout>
  )
}

export const query = graphql`
  query {
    allMdx(sort: {fields: frontmatter___date, order: DESC},filter: {frontmatter: {blog: {eq: true}}}) {
      nodes {
        frontmatter {
          date(formatString: "MMMM D, YYYY")
          title
          tag
          abstract
        }
        id
        slug
      }
    }
  }
`
export default BlogPage