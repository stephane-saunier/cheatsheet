import * as React from 'react'
import { graphql } from 'gatsby'

import Layout from '../components/layout'



const CuisinePage = ({ data }) => {
  return (
    <Layout pageTitle="	&laquo;Recette&raquo; de Cuisine">
      {
        data.allMdx.nodes.map((node) => (
          <a href={`/cuisine/${node.slug}`}>
            <div className="mt-2 tile is-parent">
              <article className="tile is-child box" key="{node.id}">
                <h2 className="title is-5">
                  {node.frontmatter.title}
                </h2>
                <p>{node.excerpt}</p>
                <br />

                <div className="columns mt-1">
                  <div className='column is-half'>
                    <p>Temps de préparation : {node.frontmatter.preparationTime}</p>
                    <p>Temps de cuisson: {node.frontmatter.cookTime}</p>
                  </div>
                  <span className="column is-quarter"></span>
                  <div className="tags has-addons">
                    <span className="tag is-black">{node.frontmatter.tag.split(",").length > 0 ? node.frontmatter.tag.split(",")[0] : ''}</span>
                    <span className="tag is-primary">{node.frontmatter.tag.split(",").length > 1 ? node.frontmatter.tag.split(",")[1] : ''}</span>
                  </div>
                </div>
              </article>
            </div>
          </a>
        ))
      }
    </Layout>
  )
}

export const query = graphql`
  query {
    allMdx(sort: {fields: frontmatter___date, order: DESC}, filter: {frontmatter: {cuisine: {eq: true}}}) {
      nodes {
        frontmatter {
          date(formatString: "MMMM D, YYYY")
          title
          tag
          cookTime
          preparationTime
        }
        id
        slug
        excerpt(truncate: true, pruneLength: 255)
      }
    }
  }
`
export default CuisinePage