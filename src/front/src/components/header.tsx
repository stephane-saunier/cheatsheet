import * as React from 'react'
import { Link, useStaticQuery, graphql } from 'gatsby'

const Header = ({ pageTitle }) => {
    const data = useStaticQuery(graphql`
    query{
        site {
          siteMetadata {
            title
          }
        }
      }
  `)


    return (
        <div className="container">
            <div className="is-full">
                <nav>
                    <ul className="navbar-link">
                        <li className="navbar-item">
                            <Link to="/">
                                Home
                            </Link>
                        </li>
                        <li className="navbar-item">
                            <Link to="/blog">
                                Blog
                            </Link>
                        </li>
                        <li className="navbar-item">
                            <Link to="/cuisine">
                                Cuisine
                            </Link>
                        </li>
                        <li className="navbar-item">
                            <Link to="/about">
                                About
                            </Link>
                        </li>
                        <li className='navbar-end'>
                            🦊{pageTitle} | {data.site.siteMetadata.title}
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    )
}

export default Header