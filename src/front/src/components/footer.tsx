import * as React from 'react'

const Footer = () => {
     return (
        <div className="container">
            <div className="is-full footer">
                <div className="content has-text-centered">
                    <p>
                        <strong>Cheatsheet</strong> by Stéphane Saunier.
                    </p>
                </div>
            </div>
        </div >
    )
}

export default Footer