/** @type {import(`gatsby).GatsbyConfig} */
module.exports = {
  siteMetadata: {
    title: `Home`,
    description: `This site hosts serveral articles from gitlab issues`,
    siteUrl: `https://stephane-saunier.gitlab.io/cheatsheet`
  },

  plugins: [`gatsby-plugin-sass`, `gatsby-plugin-image`, `gatsby-plugin-sitemap`, `gatsby-plugin-image`,
    `gatsby-plugin-sharp`, {
      resolve: `gatsby-plugin-manifest`,
      options: {
        "icon": `./src/images/icon.png`
      }
    }, `gatsby-plugin-sharp`, `gatsby-transformer-sharp`, {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `./src/images/`
      },
      __key: `images`
    }, {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `pages`,
        path: `./src/pages/`
      },
      __key: `pages`
    }, {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `blog`,
        path: `${__dirname}/blog`,
      },
      __key: `blogs`
    }, {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `cuisine`,
        path: `${__dirname}/cuisine`,
      },
      __key: `cuisine`
    }, {
      resolve: `gatsby-plugin-mdx`,
      options: {
        gatsbyRemarkPlugins: [`gatsby-remark-bulma`,`gatsby-remark-graphviz`]
      }
    },
  ]
};