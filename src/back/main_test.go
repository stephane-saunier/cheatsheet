package main

import (
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func TestGitlabCallWithoutPath(t *testing.T) {
	if testing.Short() {
		println("skipping")
		t.Skip()
	}
	var toto []string
	err := listGitlabGet(nil, "", toto)
	assert.EqualErrorf(t, err, "<< callGetGitlab(), Path should not be null", "Error should be: %v, got: %v", nil, err)
}

func TestGitlabCallNoProjectId(t *testing.T) {
	if testing.Short() {
		println("skipping")
		t.Skip()
	}
	t.Setenv("GITLAB_URL", "totolasticot")
	t.Setenv("GITLAB_PUBLIC_TOKEN", "1")
	var toto []string
	err := listGitlabGet(new(resp), "path", toto)
	assert.EqualErrorf(t, err, "<< callGetGitlab(), one configuration item is missing Project Id: , GitlabUrl: totolasticot", "Error should be: %v, got: %v", nil, err)
}

func TestGitlabCallNoGitlabUrl(t *testing.T) {
	if testing.Short() {
		println("skipping")
		t.Skip()
	}
	t.Setenv("GITLAB_PUBLIC_TOKEN", "1")
	t.Setenv("PROJECT_ID", "1")
	var toto []string
	err := listGitlabGet(new(resp), "path", toto)
	assert.EqualErrorf(t, err, "<< callGetGitlab(), one configuration item is missing Project Id: 1, GitlabUrl: ", "Error should be: %v, got: %v", nil, err)
}

func TestGitlabCallNoToken(t *testing.T) {
	if testing.Short() {
		println("skipping")
		t.Skip()
	}
	t.Setenv("GITLAB_URL", "toto")
	t.Setenv("PROJECT_ID", "1")
	var toto []string
	err := listGitlabGet(new(resp), "path", toto)
	assert.EqualErrorf(t, err, "<< callGetGitlab(), one configuration item is missing Project Id: 1, GitlabUrl: toto", "Error should be: %v, got: %v", nil, err)
}

type resp struct{}

func (r resp) Write([]byte) (int, error)  { return 0, nil }
func (r resp) Header() http.Header        { return nil }
func (r resp) WriteHeader(statusCode int) {}
