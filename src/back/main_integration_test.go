package main

import (
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestRequestWithoutMandatoryConfiguration(t *testing.T) {
	if testing.Short() {
		println("skipping")
		t.Skip()
	}

	req, err := http.NewRequest("GET", "/issues", nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(ListIssuesInProject)

	handler.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusInternalServerError, rr.Code)
}

func TestRequestLabels(t *testing.T) {
	if testing.Short() {
		println("skipping")
		t.Skip()
	}
	t.Setenv("GITLAB_URL", "https://totolasticot")
	t.Setenv("PROJECT_ID", "1")
	t.Setenv("GITLAB_PUBLIC_TOKEN", "1")

	req, err := http.NewRequest("GET", "/labels", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(ListIssuesInProject)

	handler.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusOK, rr.Code)
	assert.Equal(t, "json", rr.Body.String())

}
