package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

func main() {
	handleRequests()
}

func handleRequests() {
	http.HandleFunc("/issues", ListIssuesInProject)
	http.HandleFunc("/labels", ListLabelsInProject)
	httpPort := "8080"
	fmt.Println("Listening request on port " + httpPort)
	log.Fatal(http.ListenAndServe(":"+httpPort, nil))
}

func ListIssuesInProject(w http.ResponseWriter, r *http.Request) {
	var result []Issue
	err := listGitlabGet(w, "/issues", result)
	if err != nil {
		fmt.Println("<< ListIssuesInProject()n, Error:", err)
		return
	}
}

func ListLabelsInProject(w http.ResponseWriter, r *http.Request) {
	var result []Label
	err := listGitlabGet(w, "/labels", result)
	if err != nil {
		fmt.Println("<< ListLabelsInProject()n, Error:", err)
		return
	}
}

func listGitlabGet[ReturnType any](w http.ResponseWriter, path string, response []ReturnType) error {
	fmt.Println(">> start callGetGitlab()")
	if path == "" {
		return errors.New("<< callGetGitlab(), Path should not be null")
	}
	ProjectId := os.Getenv("PROJECT_ID")
	Token := os.Getenv("GITLAB_PUBLIC_TOKEN")
	GitlabUrl := os.Getenv("GITLAB_URL")
	BaseUrl := GitlabUrl + "/projects/" + ProjectId

	if ProjectId == "" || Token == "" || GitlabUrl == "" {
		w.WriteHeader(http.StatusInternalServerError)
		return errors.New(fmt.Sprintf("<< callGetGitlab(), one configuration item is missing Project Id: %s, GitlabUrl: %s", ProjectId, GitlabUrl))
	}

	url := BaseUrl + path
	method := "GET"
	client := &http.Client{}
	req, err := http.NewRequest(method, url, nil)

	if err != nil {
		return err
	}
	req.Header.Add("PRIVATE-TOKEN", Token)

	res, err := client.Do(req)
	if err != nil {
		return err
	}

	if err != nil {
		fmt.Println(err)
		return err
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			fmt.Println("<< listGitlabGet(),error:", err)
		}
	}(res.Body)
	err = getJson(res.Body, &response)
	if err != nil {
		return err
	}
	for _, element := range response {
		fmt.Println(element)
	}
	marshal, err := json.Marshal(response)
	if err != nil {
		return err
	}
	_, err = w.Write(marshal)
	if err != nil {
		return err
	}
	return nil
}

func getJson(body io.ReadCloser, target interface{}) error {
	return json.NewDecoder(body).Decode(target)
}

type Issue struct {
	Id          int      `json:"id"`
	Iid         int      `json:"iid"`
	Title       string   `json:"title"`
	Description string   `json:"description"`
	Labels      []string `json:"labels"`
	Downvotes   int      `json:"downvotes"`
	Upvotes     int      `json:"upvotes"`
	CreatedAt   string   `json:"created_at"`
	UpdatedAt   string   `json:"updated_at"`
	WebUrl      string   `json:"web_url"`
}

type Label struct {
	Id          int    `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Color       string `json:"color"`
}

func (l Label) String() string {
	return fmt.Sprintf("Id: %d, Name %s, Description %s", l.Id, l.Name, l.Description)
}

func (issue Issue) String() string {
	return fmt.Sprintf("id: %d, iid: %d, Title: %s\nDescription: %s\nLabels: %s\nDownvotes: %d\nUpvotes: %d\nCreated at: %s\nUpdated at: %s\nweb url: %s\n", issue.Id, issue.Iid, issue.Title, issue.Description,
		issue.Labels, issue.Downvotes, issue.Upvotes,
		issue.CreatedAt, issue.UpdatedAt, issue.WebUrl)
}
